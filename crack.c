#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;       // Length of MD5 hash strings
int dict_count = 0;         // number of pairs

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *g = md5(guess, strlen(guess));

    // Compare the two hashes
    if(strcmp(g, hash) == 0 ) 
     {
        return 1;
     }
        return 0;

    // Free any malloc'd memory
     free(guess);

    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
 // malloc space for entire file
 // get size of the file
 
    struct stat st;
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    
    char *file = malloc(len);
    
    
 
 // read entire file into memory
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("cant open %s for read\n", filename);
        exit(1);
        
    }
    
    fread(file,1,len,f);
 
 // replace \n with \0
    int count = 0;
    for(int i = 0; i < len; i++)
    {
        if(file[i] == '\n')
        {
           file[i] = '\0';
           count++;
        }
    }
 
 // malloc space for array of pointers
    char **line = malloc((count) * sizeof(char *));
 
 // fill in addresses
    int word = 0;
    line[word] = file; // the first word in the file
    word++;
    for(int i = 0; i < len; i++)
    {
        if(file[i] == '\0' && i+1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    
    line[word] = NULL;
    
 // return address of second array
    //*size = word-1;
    return line;
}





// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    //open file
    FILE *f = fopen(filename, "r");
    if (!f)
        {
        printf("cannot read from file dictionary\n");
        exit(1);
        }
    
    int arraylen = 0;
    char **dict = NULL;
    char buffer[PASS_LEN];
    //read and put into strings until end of file
    while (fgets(buffer, PASS_LEN, f) != NULL)
        {
        if (dict_count == arraylen)
            {
            arraylen += 100;
            char **newdict = realloc(dict, arraylen * sizeof(char*));
           
            dict = newdict;
            }
        
        buffer[strlen(buffer) - 1] = '\0';
        char *str = strdup(buffer);
        
        char list[(HASH_LEN + PASS_LEN + 2)];
        sprintf(list, "%s:%s", md5(str, strlen(str)), str);
        
        dict[dict_count] = strdup(list);
        dict_count++;
        
        }
    
    if (dict_count == arraylen)
        {
        char **newarray = realloc(dict, (arraylen + 1) * sizeof(char*));
        
        dict = newarray;
        }
    
    dict[dict_count] = NULL;
    return dict;
     
     
}
int quickcompare( const void *a, const void *b)
    {
    return strncmp( *((char **)a), *((char **)b), HASH_LEN);
    }

int binarycompare(const void *key, const void *element)
    {
     return strncmp( (char *)key, *((char **)element), HASH_LEN - 1);
    }


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dict_count, sizeof(char *), quickcompare);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    for( int i = 0; hashes[i] != NULL; i++)
        {
        char **found = bsearch( hashes[i], dict, dict_count, sizeof(char *), binarycompare);
        
        if (found != NULL)
            {
            printf("%s\n", *found);
            }
        }
        
        free(hashes);
        free(dict);
}
